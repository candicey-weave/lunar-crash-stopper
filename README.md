# Lunar Crash Stopper
- Stop Lunar Client from crashing when you want to kill its process.

*Note: Use [LibInjector](https://gitlab.com/candicey-weave/libinjector) to inject.*
