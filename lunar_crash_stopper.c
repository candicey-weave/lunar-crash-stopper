#include <stddef.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

const char *STATE_LIST[] = {
  // "LUNARCLIENT_STATUS_BUILD_CACHE",
  "LUNARCLIENT_STATUS_PREINIT",
  "LUNARCLIENT_STATUS_INIT",
  "Lunar.start",
  "Launching",
  "LUNARCLIENT_STATUS_STARTED"
};

void inject() {
  printf("[LCS] Injected!\n");

  size_t state_list_size = sizeof(STATE_LIST) / sizeof(STATE_LIST[0]);
  for (int i = 0; i < state_list_size; i++) {
    printf("%s\n", STATE_LIST[i]);
  }

  printf("[LCS] Exiting...\n");

  exit(0);
}

void *libLoad(void *arg) {
  inject();
  return NULL;
}

__attribute__((constructor))
void init() {
  pthread_t thread;
  pthread_create(&thread, NULL, libLoad, NULL);
  pthread_detach(thread);
}
